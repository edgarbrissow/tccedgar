-- CREATE SCHEMA `senaiagenda` ;

CREATE  TABLE `senaiagenda`.`contatos` (

  `id` INT NOT NULL AUTO_INCREMENT ,

  `nome` VARCHAR(45) NULL ,

  `telefone` VARCHAR(45) NULL ,

  `email` VARCHAR(45) NULL ,

  `cidade` VARCHAR(45) NULL ,

  PRIMARY KEY (`id`) );