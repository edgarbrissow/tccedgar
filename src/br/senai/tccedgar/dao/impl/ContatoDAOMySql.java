package br.senai.tccedgar.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.senai.tccedgar.dao.Conexao;
import br.senai.tccedgar.dao.ContatoDAO;
import br.senai.tccedgar.entidade.Contato;

public class ContatoDAOMySql implements ContatoDAO { // explique porque está gerando este erro.
	//explique porque usamos estes atributos constantes na classe.
	private static final String INSERT = "INSERT INTO CONTATOS"
			+ "(NOME, TELEFONE, EMAIL, CIDADE) VALUE (?,?,?,?)";
	private static final String GET_BY_ID = "SELECT * FROM CONTATOS WHERE ID = ?";
	private static final String GET_ALL = "SELECT * FROM CONTATOS";
	private static final String DELETAR = "DELETE FROM CONTATOS WHERE ID = ? ";
	private static final String UPDATE = "UPDATE CONTATOS SET NOME = ?, TELEFONE=?, EMAIL=?, CIDADE=?  WHERE ID=?";

	//se cinseguirem implementem o metodo getById()
	@Override
	public Contato getById(Integer id){
		Contato retorno = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = Conexao.conectar();
			ps =  con.prepareStatement(GET_BY_ID);
			rs = ps.executeQuery();
			ps.setInt(1, id);
			if(rs.next()){
				retorno = new Contato();
				retorno.setId(rs.getInt("ID"));
				retorno.setNome(rs.getString("NOME"));
				retorno.setTelefone(rs.getString("TELEFONE"));
				retorno.setEmail(rs.getString("EMAIL"));
				retorno.setCidade(rs.getString("CIDADE"));

			}

			ps.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Conexao.desconectar(con);
		}

		return retorno;
	}


	//Expliquem como funciona o getAll(), o salvar(), deletar()
	@Override
	public List<Contato> getAll() {
		List<Contato> lista = new ArrayList<Contato>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		con = Conexao.conectar();
		try {
			ps = (PreparedStatement) con.prepareStatement(GET_ALL);
			rs = ps.executeQuery();
			while (rs.next()) {
				Contato c = new Contato();
				c.setId(rs.getInt("ID"));//coloque a String que for necessária 
				c.setNome(rs.getString("NOME"));//coloque a String que for necessária 
				c.setTelefone(rs.getString("TELEFONE"));//coloque a String que for necessária 
				c.setEmail(rs.getString("EMAIL"));//coloque a String que for necessária 
				c.setCidade(rs.getString("CIDADE"));//coloque a String que for necessária 
				lista.add(c);
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lista;
	}

	@Override
	public Contato salvar(Contato contato) {
		Contato retorno = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet generatedKeys = null;
		if (contato.getId() == null) { //explique porque a utilização do try / catch e a verificação do ID.
			try {
				con = Conexao.conectar();
				ps = (PreparedStatement) con.prepareStatement(INSERT,
						PreparedStatement.RETURN_GENERATED_KEYS);
				ps.setString(1, contato.getNome());//coloque o método da Classe Contato que for necessário
				ps.setString(2, contato.getTelefone());//coloque o método da Classe Contato que for necessário
				ps.setString(3, contato.getEmail());//coloque o método da Classe Contato que for necessário
				ps.setString(4, contato.getCidade());//coloque o método da Classe Contato que for necessário
				ps.executeUpdate();
				generatedKeys = ps.getGeneratedKeys();
				generatedKeys.next();
				contato.setId(generatedKeys.getInt(1));
				ps.close();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				Conexao.desconectar(con);
			}
		} else {
			try {
				con = Conexao.conectar();
				ps = (PreparedStatement) con.prepareStatement(UPDATE);
				ps.setString(1, contato.getNome());
				ps.setString(2, contato.getTelefone());
				ps.setString(3, contato.getEmail());
				ps.setString(4, contato.getCidade());
				ps.setInt(5, contato.getId());
				ps.executeUpdate();
				ps.close();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				Conexao.desconectar(con);
			}
		}
		return retorno;
	}

	@Override
	public Boolean excluir(Contato contato) {
		Boolean retorno = false;
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = Conexao.conectar();
			ps = (PreparedStatement) con.prepareStatement(DELETAR);
			ps.setInt(1, contato.getId());
			ps.executeUpdate();
			retorno = true;
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			Conexao.desconectar(con);
		}
		return retorno;
	}

	@Override
	public Boolean autenticar(String user, String senha) {
		Boolean retorno = false;
		if (user.equals("senai") && senha.equals("123")) {
			retorno = true;
		}
		return retorno;
	}
}
