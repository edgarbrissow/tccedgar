package br.senai.tccedgar.dao;
import br.senai.tccedgar.entidade.Contato;

import java.util.List;



public interface ContatoDAO {

    public Contato getById(Integer id);

    public List<Contato> getAll();

    public Contato salvar(Contato contato);

    public Boolean excluir(Contato contato);

    public Boolean autenticar(String user, String senha);
}
