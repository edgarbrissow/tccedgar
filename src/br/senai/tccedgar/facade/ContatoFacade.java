package br.senai.tccedgar.facade;

import java.util.List;

import br.senai.tccedgar.dao.ContatoDAO;
import br.senai.tccedgar.dao.impl.ContatoDAOMySql;
import br.senai.tccedgar.entidade.Contato;

public class ContatoFacade {

    ContatoDAO dao = new ContatoDAOMySql(); // expelique o que está acontecendo aqui!!

    public List<Contato> getAll() {
        return dao.getAll();
    }

    public Contato getById(Integer id) {
        return dao.getById(id);
    }

    public Boolean excluir(Contato contato) {
        return dao.excluir(contato);
    }

    public Contato salvar(Contato contato) {
        return dao.salvar(contato);
    }
}
