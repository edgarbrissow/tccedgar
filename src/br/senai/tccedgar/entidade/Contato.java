package br.senai.tccedgar.entidade;

public class Contato {
	private Integer id;
	private String nome;
	private String telefone;
	private String email;
	private String cidade;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}





	//implementem o código da classe conforme a tabela do banco e conforme as instancias de
	//objetos usados no outros códigos do sistema.
}
